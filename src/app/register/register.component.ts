import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { RegisterDialogComponent } from "./dialog/register-dialog.component";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  constructor(public dialog: MatDialog) {}

  openDialog() {
    this.dialog.open(RegisterDialogComponent);
  }
}
