import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {Router} from "@angular/router";
import {MatDialogRef} from "@angular/material/dialog";

import {UsersService} from "../../users/users.service";
import {IUser} from "../../users/IUser";
import {Common} from "../../common/common";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['../register.component.scss']
})

export class RegisterDialogComponent {

  constructor(
    private navigator: Router,
    private userService: UsersService,
    public dialogRef: MatDialogRef<RegisterDialogComponent>,
    private snackBar: MatSnackBar)
  {}

  // ---------------------------------------------------------

  register_form = new FormGroup({
    // This regex checks for a single word that have only alphabet characters and numbers.
    username: new FormControl("", [Validators.required, Validators.pattern("[A-Za-z0-9]+$")]),
    firstname: new FormControl("", Validators.required),
    lastname: new FormControl(""),
    password: new FormControl("", Validators.required)
  });

  get username()         { return this.register_form.get("username");         }
  get firstname()        { return this.register_form.get("firstname");        }
  get lastname()         { return this.register_form.get("lastname");         }
  get password()         { return this.register_form.get("password");         }

  // ---------------------------------------------------------

  getUsernameErrorMessage() {
    if (this.username?.hasError("required")) {
      return "You must enter a username.";
    }

    if(this.username?.hasError("pattern")) {
      return "Username format is incorrect.";
    }

    return "";
  }

  getFirstnameErrorMessage() {
    if (this.firstname?.hasError("required")) {
      return "You must enter a firstname.";
    }

    return "";
  }

  getPasswordErrorMessage() {
    if(this.password?.hasError("required")) {
      return "You must enter a password.";
    }

    return "";
  }

  // ---------------------------------------------------------

  private hasErrors() {
    return this.username?.errors ||
      this.firstname?.errors     ||
      this.password?.errors      ||
      this.password?.errors;
  }

  // ---------------------------------------------------------

  private convertToIUser(): IUser {
    return {
      id: Common.generateID(),
      username: this.username?.value,
      firstname: this.firstname?.value,
      lastname: this.lastname?.value,
      password: this.password?.value
    };
  }

  // ---------------------------------------------------------

  onRegisterEvent() {
    if(!this.hasErrors()) {
      let result = this.userService.register(this.convertToIUser());
      if(!result) {
        this.snackBar.open("The user already exists.", "Close", {horizontalPosition: "center", verticalPosition: "top"});
        this.username?.setErrors(["required"]);
        return;
      }

      this.snackBar.open("User added.", "Close", {horizontalPosition: "center", verticalPosition: "top"});
      this.dialogRef.close();
    }
  }
}
