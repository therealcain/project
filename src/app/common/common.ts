export class Common
{
  static generateID() {
    // Algorithm taken from StackOverflow.
    return Date.now().toString(36) + Math.random().toString(36).substr(2);
  }

}
