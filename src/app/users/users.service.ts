import { BehaviorSubject } from "rxjs";
import { Injectable } from "@angular/core";
import { IUser } from "./IUser";

@Injectable({
  providedIn: "root"
})
export class UsersService {
  user$ = new BehaviorSubject<IUser | null>(this.getUser());

  // ---------------------------------------------------------

  getUser() {
    let userFromLS = localStorage.getItem("user");
    return userFromLS !== null ? JSON.parse(userFromLS) : null;
  }

  get allUsers() {
    let userFromLS = localStorage.getItem("allUsers");
    return userFromLS !== null ? JSON.parse(userFromLS) : [];
  }

  set allUsers(users: IUser[]) {
    localStorage.setItem("allUsers", JSON.stringify(users));
  }

  // ---------------------------------------------------------

  private userExists(username: string) {
    return this.allUsers.find((user) => {
      return user.username === username;
    });
  }

  // ---------------------------------------------------------

  login(username: string, password: string) {
    let user = this.userExists(username);
    if(user !== undefined && user?.password === password) {
      localStorage.setItem("user", JSON.stringify(user));
      this.user$.next(user);
      return true;
    }

    return false;
  }

  register(form: IUser) {
    let user = this.userExists(form.username);
    if(user === undefined) {
      this.allUsers = [form, ...this.allUsers];
      return true;
    }

    return false;
  }

  logout() {
    localStorage.removeItem("user");
    this.user$.next(null);
  }
}
