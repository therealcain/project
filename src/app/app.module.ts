import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from "./material/material.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RegisterDialogComponent } from './register/dialog/register-dialog.component';
import { HomeComponent } from './home/home.component';
import { AddAssignmentDialogComponent } from './home/assignment/add/dialog/add-assignment-dialog.component';
import { AddAssignmentComponent } from './home/assignment/add/add-assignment.component';
import { AssignmentTable } from './home/assignment/table/assignment-table';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {ModifyAssignmentDialogComponent} from "./home/assignment/modify/dialog/modify-assignment-dialog.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    RegisterDialogComponent,
    HomeComponent,
    AddAssignmentDialogComponent,
    AddAssignmentComponent,
    AssignmentTable,
    ModifyAssignmentDialogComponent
  ],
  entryComponents: [
    RegisterDialogComponent, AddAssignmentDialogComponent, ModifyAssignmentDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
