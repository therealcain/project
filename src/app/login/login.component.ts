import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { UsersService } from "../users/users.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  login_form = new FormGroup({
    username: new FormControl("", Validators.required),
    password: new FormControl("", Validators.required)
  });

  // ---------------------------------------------------------

  constructor(private userService: UsersService, private navigator: Router)
  { }

  // ---------------------------------------------------------

  get username() { return this.login_form.get("username"); }
  get password() { return this.login_form.get("password"); }

  // ---------------------------------------------------------

  getUsernameErrorMessage() {
    if (this.username?.hasError("required"))
      return "You must enter a username.";

    return "";
  }

  getPasswordErrorMessage() {
    if (this.password?.hasError("required"))
      return "You must enter a password.";

    return "";
  }

  // ---------------------------------------------------------

  private hasErrors() {
    return this.username?.errors||
      this.password?.errors;
  }

  // ---------------------------------------------------------

  onLoginEvent() {
    if(!this.hasErrors())
    {
      let success = this.userService.login(this.username?.value, this.password?.value);
      if(!success) {
        this.username?.setErrors(["required"]);
        this.password?.setErrors(["required"]);
        return;
      }

      this.navigator.navigate([""]);
    }
  }
}
