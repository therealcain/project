import {Component, TemplateRef} from '@angular/core';
import { UsersService } from "../users/users.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent{

  constructor(public navigator: Router, public usersService: UsersService)
  { }

  onLogoutEvent() {
    this.usersService.logout();
    this.navigator.navigate(["login"]);
  }
}
