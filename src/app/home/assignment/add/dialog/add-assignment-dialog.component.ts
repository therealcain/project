import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDialogRef} from "@angular/material/dialog";

import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';
import {Common} from "../../../../common/common";
import {UsersService} from "../../../../users/users.service";
import {IAssignment} from "../../IAssignment";
import {AssignmentsService} from "../../assignments.service";
import {MatSnackBar} from "@angular/material/snack-bar";

const moment = _rollupMoment || _moment;

@Component({
  selector: 'app-add-assignment-dialog',
  templateUrl: './add-assignment-dialog.component.html',
  styleUrls: ['../../assignment.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ]
})
export class AddAssignmentDialogComponent {

  constructor(
    private usersService: UsersService,
    private assignmentsService: AssignmentsService,
    private dialogRef: MatDialogRef<AddAssignmentDialogComponent>,
    private snackBar: MatSnackBar)
  {
  }

  // ---------------------------------------------------------

  add_assignment_form = new FormGroup({
    title: new FormControl("", Validators.required),
    content: new FormControl("", Validators.required),
    priority: new FormControl("", Validators.required),
    status: new FormControl("", Validators.required),
    deadline: new FormControl("", Validators.required),
    author: new FormControl(this.getUsernames()[0], Validators.required)
  });

  get title()    { return this.add_assignment_form.get("title") as FormControl;    }
  get content()  { return this.add_assignment_form.get("content") as FormControl;  }
  get priority() { return this.add_assignment_form.get("priority") as FormControl; }
  get status()   { return this.add_assignment_form.get("status") as FormControl;   }
  get deadline() { return this.add_assignment_form.get("deadline") as FormControl; }
  get author()   { return this.add_assignment_form.get("author") as FormControl;   }

  // ---------------------------------------------------------

  private convertToIAssignment(): IAssignment {
    return {
      id: Common.generateID(),
      title: this.title.value,
      content: this.content.value,
      priority: this.priority.value,
      status: "Pending",
      date: this.getCurrentDate(),
      deadline: this.formatDate(this.deadline.value),
      author: this.author.value
    };
  }

  private hasErrors() {
    return this.title?.errors ||
      this.content?.errors    ||
      this.priority?.errors   ||
      this.deadline?.errors   ||
      this.author?.errors;
  }

  onEventAdd() {
    if(!this.hasErrors()) {
      this.assignmentsService.addAssignment(this.convertToIAssignment());
      this.snackBar.open("Assignment added.", "Close");
      this.dialogRef.close();
    }
  }

  // ---------------------------------------------------------

  getUsernames() {
    return this.usersService.allUsers.map(user => user.username);
  }

  // ---------------------------------------------------------

  private formatDate(date) {
    return moment(date).format('MM/DD/YYYY');
  }

  getCurrentDate() {
    return this.formatDate(Date.now());
  }
}
