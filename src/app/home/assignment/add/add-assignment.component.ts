import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddAssignmentDialogComponent } from "./dialog/add-assignment-dialog.component";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-assignment',
  templateUrl: './add-assignment.component.html',
  styleUrls: ['../assignment.scss']
})

export class AddAssignmentComponent{
  constructor(public dialog: MatDialog) {}

  openDialog() {
    this.dialog.open(AddAssignmentDialogComponent);
  }
}
