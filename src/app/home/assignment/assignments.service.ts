import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

import { IAssignment } from "./IAssignment"
import {Common} from "../../common/common";

@Injectable({
  providedIn: 'root'
})
export class AssignmentsService {

  assignments$ = new BehaviorSubject<IAssignment[]>(this.allAssignments);

  // ---------------------------------------------------------

  get allAssignments(): IAssignment[] {
    let assignmentFromLS = localStorage.getItem("allAssignments");
    return assignmentFromLS !== null ? JSON.parse(assignmentFromLS) : [];
  }

  set allAssignments(other: IAssignment[]) {
    localStorage.setItem("allAssignments", JSON.stringify(other));
    this.assignments$.next(other);
  }

  // ---------------------------------------------------------

  private findIndex(assignment: IAssignment) {
    return this.allAssignments.findIndex(e => e.id === assignment.id);
  }

  // ---------------------------------------------------------

  modifyTitle(assignment: IAssignment, title: string) {
    let allAssignments = this.allAssignments;
    allAssignments[this.findIndex(assignment)].title = title;
    this.allAssignments = allAssignments;
  }

  modifyContent(assignment: IAssignment, content: string) {
    let allAssignments = this.allAssignments;
    allAssignments[this.findIndex(assignment)].content = content;
    this.allAssignments = allAssignments;
  }

  changePriority(assignment: IAssignment, priority: string) {
    let allAssignments = this.allAssignments;
    allAssignments[this.findIndex(assignment)].priority = priority;
    this.allAssignments = allAssignments;
  }

  changeStatus(assignment: IAssignment, status: string) {
    let allAssignments = this.allAssignments;
    allAssignments[this.findIndex(assignment)].status = status;
    this.allAssignments = allAssignments;
  }

  modifyDeadline(assignment: IAssignment, deadline: string) {
    let allAssignments = this.allAssignments;
    allAssignments[this.findIndex(assignment)].deadline = deadline;
    this.allAssignments = allAssignments;
  }

  // ---------------------------------------------------------

  addAssignment(assignment: IAssignment) {
    this.allAssignments = [assignment, ...this.allAssignments];
  }

  removeAssignment(assignment: IAssignment) {
    this.allAssignments = this.allAssignments.filter(e => e.id !== assignment.id);
  }

  // ---------------------------------------------------------

  static saveCurrentAssignment(assignment: IAssignment) {
    localStorage.setItem("currentAssignment", JSON.stringify(assignment));
  }

  static loadCurrentAssignment() {
    let assignmentFromLS = localStorage.getItem("currentAssignment");
    return assignmentFromLS !== null ? JSON.parse(assignmentFromLS) : [];
  }

}
