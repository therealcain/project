import {Component, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from "@angular/animations";
import {MatTableDataSource} from "@angular/material/table";
import {AssignmentsService} from "../assignments.service";
import {MatSort} from "@angular/material/sort";
import {IAssignment} from "../IAssignment";
import {MatDialog} from "@angular/material/dialog";
import {ModifyAssignmentDialogComponent} from "../modify/dialog/modify-assignment-dialog.component";

@Component({
  selector: 'app-assignment-table',
  templateUrl: './assignment-table.html',
  styleUrls: ['../assignment.scss', 'assignment-table.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class AssignmentTable {

  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;

  columnsToDisplay: string[] = ["title", "priority", "status", "date", "deadline", "author", "modify"];
  expandedElement: IAssignment | null = null;

  // ---------------------------------------------------------

  constructor(private assignmentsService: AssignmentsService, public dialog: MatDialog)
  { }

  // ---------------------------------------------------------

  getDataSource(): MatTableDataSource<IAssignment> {
    let dataSource = new MatTableDataSource(this.assignmentsService.assignments$.value);
    dataSource.sort = this.sort;
    return dataSource;
  }

  // ---------------------------------------------------------

  getBasedOnColumnAndElement(element, column) {
    return {
      "title": element.title,
      "priority": element.priority,
      "status": element.status,
      "date": element.date,
      "deadline": element.deadline,
      "author": element.author
    }[column];
  }

  // ---------------------------------------------------------

  openModifyDialog(element) {
    AssignmentsService.saveCurrentAssignment(element);
    this.dialog.open(ModifyAssignmentDialogComponent);
  }

}
