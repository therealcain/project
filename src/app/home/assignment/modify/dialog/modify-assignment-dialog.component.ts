import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDialogRef} from "@angular/material/dialog";

import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';
import {UsersService} from "../../../../users/users.service";
import {AssignmentsService} from "../../assignments.service";
import {Common} from "../../../../common/common";
import {IAssignment} from "../../IAssignment";
import {MatSnackBar} from "@angular/material/snack-bar";

const moment = _rollupMoment || _moment;

@Component({
  selector: 'app-modify-assignment-dialog',
  templateUrl: './modify-assignment-dialog.component.html',
  styleUrls: ['../../assignment.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ]
})
export class ModifyAssignmentDialogComponent {

  assignment: IAssignment = AssignmentsService.loadCurrentAssignment();

  constructor(private usersService: UsersService,
    private assignmentsService: AssignmentsService,
    private dialogRef: MatDialogRef<ModifyAssignmentDialogComponent>,
    private snackBar: MatSnackBar)
  {
  }

  // ---------------------------------------------------------

  modify_assignment_form = new FormGroup({
    title: new FormControl(this.assignment.title, Validators.required),
    content: new FormControl(this.assignment.content, Validators.required),
    priority: new FormControl(this.assignment.priority, Validators.required),
    status: new FormControl(this.assignment.status, Validators.required),
    deadline: new FormControl(this.getDateFromString(this.assignment.deadline), Validators.required)
  });

  get title()    { return this.modify_assignment_form.get("title") as FormControl;    }
  get content()  { return this.modify_assignment_form.get("content") as FormControl;  }
  get priority() { return this.modify_assignment_form.get("priority") as FormControl; }
  get status()   { return this.modify_assignment_form.get("status") as FormControl;   }
  get deadline() { return this.modify_assignment_form.get("deadline") as FormControl; }

  // ---------------------------------------------------------

  private hasErrors() {
    return this.title?.errors ||
      this.content?.errors    ||
      this.priority?.errors   ||
      this.status?.errors     ||
      this.deadline?.errors;
  }

  onEventModify() {
    if(!this.hasErrors()) {
      this.assignmentsService.modifyTitle(this.assignment, this.title.value);
      this.assignmentsService.modifyContent(this.assignment, this.content.value);
      this.assignmentsService.changePriority(this.assignment, this.priority.value);
      this.assignmentsService.changeStatus(this.assignment, this.status.value);
      this.assignmentsService.modifyDeadline(this.assignment, this.formatDate(this.deadline.value));

      this.snackBar.open("Assignment has been modified.", "Close");
      this.dialogRef.close();
    }
  }

  onEventDelete() {
    this.assignmentsService.removeAssignment(this.assignment);
    this.snackBar.open("Assignment has been deleted.", "Close");
    this.dialogRef.close();
  }

  // ---------------------------------------------------------

  private formatDate(date) {
    return moment(date).format('MM/DD/YYYY');
  }

  private getDateFromString(date: string) {
    return moment(date, "MM/DD/YYYY").toDate();
  }
}
