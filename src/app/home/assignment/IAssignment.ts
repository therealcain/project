export interface IAssignment {
  id: string;
  title: string;
  content: string;
  priority: string;
  status: string;
  date: string;
  deadline: string;
  author: string;
}
